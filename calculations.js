"use strict";

// Global variables within this file (not directory)
let num1 = '';
let num2 = '';
let operator = '';
let final_result = 0.0;
let setting_left_num = true;


// Reads in left and right operand
function captureNumber(num){

    if(setting_left_num === true)
    {
        num1 = num1 + num;
        console.log(num1);
        box1.innerHTML = num1;
    }
    else
    {
        num2 = num2 + num;
        console.log(num2);
        box2.innerHTML = num2;
    }
}

// Reads in operator
function captureOperator(input_operator)
{
    setting_left_num = false;
    box3.innerHTML = input_operator;
    console.log(input_operator);

    if (input_operator === "/") {operator = "/"}
    else if (input_operator === "x") { operator = "x"}
    else if (input_operator === "-") { operator = "-"}
    else if (input_operator === "+") { operator = "+"}
}

// Clears the model of the calculator
function clearCalculator(){
    num1 = '';
    box1.innerHTML = '';
    num2 = '';
    box3.innerHTML = '';
    operator = '';
    box2.innerHTML = '';
    setting_left_num = true;
}

// Computes result
function returnResult()
{
    let result = 0;

    if (operator === "/") { result = parseFloat(num1) / parseFloat(num2) }
    else if (operator === "x") { result = parseFloat(num1) * parseFloat(num2) }
    else if (operator === "-") { result = parseFloat(num1) - parseFloat(num2) }
    else if (operator === "+") { result = parseFloat(num1) + parseFloat(num2) }

    clearCalculator();
    final_result = result;
    box3.innerHTML = final_result;
}

// Changes left and right operands to negative and vice-versa
function changeSign()
{
    if (setting_left_num === true)
    {
        if (num1 != '') {
            num1 = parseFloat(num1) * -1;
            box1.innerHTML = num1;
        }

    }
    else
    {
        if (num2 != '') {
            num2 = parseFloat(num2) * -1;
            box2.innerHTML = num2;
        }
    }
}
